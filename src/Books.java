public class Books {
    public Integer getBookPnr() {
        return bookPnr;
    }

    public void setBookPnr(Integer bookPnr) {
        this.bookPnr = bookPnr;
    }

    public Books(Integer bookPnr, String bookName) {
        this.bookPnr = bookPnr;
        this.bookName = bookName;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    private Integer bookPnr;
    private String bookName;
    private Integer price=10;

}
