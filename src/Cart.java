import java.util.ArrayList;

public class Cart {
    ArrayList<Books> listOfBooks=new ArrayList<>();
    private Integer totalPrice;

    public void addToCart(Books books){
        listOfBooks.add(books);
    }
    public void showTotalCartAmount(){
        Integer toatalAmount=0;
        for (Books books: listOfBooks) {
            toatalAmount=toatalAmount+books.getPrice();
        }
        System.out.println("Total Cart Amount: "+toatalAmount);
    }
}
