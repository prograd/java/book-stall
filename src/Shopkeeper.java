public class Shopkeeper {
    public static void main(String[] args) {
        Books book1=new Books(1234,"Rema Thareja");
        Books book2=new Books(1235,"Hellen Killer");
        Books book3=new Books(1236,"C language");

        Cart cart=new Cart();
        cart.addToCart(book1);
        cart.addToCart(book2);
        cart.addToCart(book3);

        cart.showTotalCartAmount();
    }
}
